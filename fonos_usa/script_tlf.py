import  click
import time

validar = lambda t:t>0
head = "560949"
def numeros(head):
	NROS_USA = 10
	CHARS = NROS_USA-len(head)
	MAX_VAL = int("1"+"0"*CHARS)
	NROS = list(range(MAX_VAL))
	FORMAT=f"%s%0.{CHARS}d"
	TELEFONOS=list(map(lambda s:int(FORMAT%(head, s)), NROS))
	# ~ TELEFONOS=list(map(lambda s:int(f"{head}{s}"), NROS))
	VALIDOS = list(filter(validar, TELEFONOS))
	return VALIDOS
	
	
import re

@click.command()
@click.argument("head")
def exp(head):
	regex = re.compile("^[1-9][0-9]{5}$")
	if regex.match(head):
		NUMEROS = numeros(head)
		for n in NUMEROS:
			print(n)
			time.sleep(0.2)
		return NUMEROS
	else:
		click.secho('*Atención*', bg='red', blink=True, bold=True)
		click.secho("*"*55, blink=True)
		click.secho("Debe ingresar una cifra que corresponda a 6 números", 
		bg='red', fg='white')
		click.secho("sin letras ni carácteres especiales.", 
		bg='red', fg='white')
		click.secho("*"*55, blink=True)


if __name__ == '__main__':
	exp()
